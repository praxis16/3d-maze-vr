﻿using UnityEngine;
using System.Collections;

public class FollowTheBall : MonoBehaviour {
	//public GameObject player;
	public GameObject ball;
    private Rigidbody ballRB;
	private float wantedRotationAngle, currentRotationAngle, rotationDamping = 3f;
	private Vector3 offset, vel;
	private Quaternion wantedRotation, currentRotation, finalRotation;
	// Use this for initialization
	void Start () {
		offset = transform.position - ball.transform.position;
        ballRB = ball.transform.GetComponent<Rigidbody>();
	}

	void LateUpdate () {
		
		vel = ballRB.velocity;
        vel.y = 0;
        vel = vel.normalized;
        wantedRotation = Quaternion.LookRotation(vel);
        wantedRotationAngle = wantedRotation.eulerAngles.y;
        currentRotation = Quaternion.LookRotation(ball.transform.position - transform.position);
        currentRotationAngle = currentRotation.eulerAngles.y;
        currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
        finalRotation = Quaternion.Euler(0, currentRotationAngle, 0);
		transform.position = ball.transform.position + finalRotation * offset;
		transform.LookAt (ball.transform);
        transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
	}
}
