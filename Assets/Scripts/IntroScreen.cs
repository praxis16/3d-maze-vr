﻿using UnityEngine;
using System.Collections;

public class IntroScreen : MonoBehaviour {

    int lastScore, highScore, lastMin, lastSec, bestMin, bestSec;
    float lastTime, bestTime;
    TextMesh MenuText;
    string textContent;
    // Use this for initialization
    void Start()
    {
        lastScore = PlayerPrefs.GetInt("lastScore", 0);
        highScore = PlayerPrefs.GetInt("bestScore", 0);
        lastTime = PlayerPrefs.GetFloat("lastTime", 0);
        bestTime = PlayerPrefs.GetFloat("bestTime", 0);
        lastMin = (int)lastTime / 60;
        lastSec = (int)lastTime % 60;
        bestMin = (int)bestTime / 60;
        bestSec = (int)bestTime % 60;
        MenuText = GetComponent<TextMesh>();
        textContent = string.Format("Last Score:\n{0}\nTime Taken:\n{1:00}:{2:00}\nHigh Score:\n{3}\nBest Time:\n{4:00}:{5:00}", lastScore, lastMin, lastSec, highScore, bestMin, bestSec);
        MenuText.text = textContent;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetButtonDown ("Fire1") || GvrViewer.Instance.Triggered) {
			SceneManager.LoadScene (1);
		}*/
    }

    void LateUpdate()
    {
        GvrViewer.Instance.UpdateState();
        if (GvrViewer.Instance.BackButtonPressed)
        {
            Application.Quit();
        }
    }
}
