﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BallController : MonoBehaviour {
	public float speed, volume, pitch;
    public int checkpointCount;
    public AudioClip wallImpact, checkpointFound;
    private Rigidbody rb;
    private AudioSource audio;
    public static int score;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
        audio = GetComponent<AudioSource>();
        score = 0;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name.IndexOf("checkpoint") > -1)
        {
            AudioSource.PlayClipAtPoint(checkpointFound, col.contacts[0].point);
            Destroy(col.gameObject);
            score += 10;
            checkpointCount--;
        } else if (col.gameObject.name.IndexOf("Wall") > -1)
        {
            AudioSource.PlayClipAtPoint(wallImpact, col.contacts[0].point, volume);
        }
        if(checkpointCount == 0)
        {
            float time = Timer.timeLimit - Timer.timeRemaining;
            PlayerPrefs.SetInt("lastScore", score);
            PlayerPrefs.SetFloat("lastTime", time);
            if(PlayerPrefs.GetFloat("bestTime", 0) > time || PlayerPrefs.GetFloat("bestTime") == 0)
            {
                PlayerPrefs.SetFloat("bestTime", time);
            }
            if (PlayerPrefs.GetInt("bestScore", 0) < score)
            {
                PlayerPrefs.SetInt("bestScore", score);
            }
            PlayerPrefs.Save();
            SceneManager.LoadScene(0);
        }
    }

    // Update is called while physics calculations are done
    void FixedUpdate () {
        if (audio.isPlaying) {
            volume = rb.velocity.magnitude / 3.5f;
            pitch = rb.velocity.magnitude / 3f;

            if ( volume > 1) {
                audio.volume = 1f;
            } else {
                audio.volume = volume;
            }
            if(volume == 0) {
                audio.Stop();
            }

            if(pitch > 2) {
                pitch = 2;
            }
            audio.pitch = 1 + (pitch - 1) / 10;
            
        }
        if(Input.GetButtonDown("Fire1")) {
            StopCoroutine("MoveAhead");
            StartCoroutine("MoveAhead");
        }    
	}

    IEnumerator MoveAhead()
    {
        if (!audio.isPlaying) { 
            audio.Play();
        }
        while(Input.GetButton("Fire1"))
        {
            rb.AddForce(Camera.main.transform.forward * speed);
            yield return null;
        }
    }

	//called after all general update processes
	void LateUpdate(){
		GvrViewer.Instance.UpdateState ();
		if (GvrViewer.Instance.BackButtonPressed) {
            PlayerPrefs.Save();
            SceneManager.LoadScene(0);
        }
	}
}
